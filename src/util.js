const csv = require("csvtojson");

function csvToJSON(path) {
  return csv()
    .fromFile(path)
    .then((data) => {
      return data;
    });
}
module.exports = csvToJSON;

